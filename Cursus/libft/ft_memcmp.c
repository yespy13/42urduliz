#include "libft.h"
int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			i;
	unsigned char	*s1char;
	unsigned char	*s2char;

	s1char = (unsigned char *)s1;
	s2char = (unsigned char *)s2;
	i = 0;
	while (i < n)
	{
		if (s1char[i] != s2char[i])
			return (s1char[i] - s2char[i]);
		i++;
	}
	return (0);
}
