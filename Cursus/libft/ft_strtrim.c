#include "libft.h"

int	ft_check_set(char c, char const *set)
{
	int	i;

	i = 0;
	while (set[i])
	{
		if (set[i] == c)
			return (1);
		i++;
	}
	return (0);
}

char	*ft_fill_str(int start, int end, char *s1char, char *str)
{
	int	i;

	i = 0;
	while (start < end)
	{
		str[i] = s1char[start];
		i++;
		start++;
	}
	str[i] = 0;
	return (str);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*str;
	int		start;
	int		end;
	char	*s1char;

	if (!s1 || !set)
		return (NULL);
	s1char = (char *) s1;
	start = 0;
	while (s1char[start] && ft_check_set(s1char[start], set))
		start++;
	end = ft_strlen(s1char);
	while (end > start && ft_check_set(s1char[end - 1], set))
		end--;
	str = (char *)malloc(sizeof(*s1char) * (end - start + 1));
	if (!str)
		return (NULL);
	str = ft_fill_str(start, end, s1char, str);
	return (str);
}
