#include "libft.h"

size_t	ft_strlcpy(char *dest, char *src, size_t size)
{	
	size_t	i;

	i = 0;
	if (!src || !dest)
		return (0);
	if (size > 0)
	{
		while (src[i] != '\0' && (i + 1) < size)
		{
			dest[i] = src[i];
			i++;
		}
		dest[i] = '\0';
	}
	while (src[i] != 0)
	{
		i++;
	}
	return (i);
}
