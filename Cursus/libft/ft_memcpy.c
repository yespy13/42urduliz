#include "libft.h"
void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t			i;
	unsigned char	*dstchar;
	unsigned char	*srcchar;

	if (dst == NULL && src == NULL)
		return (NULL);
	dstchar = (unsigned char *)dst;
	srcchar = (unsigned char *)src;
	i = 0;
	while (i < n)
	{
		dstchar[i] = srcchar[i];
		i++;
	}
	return (dst);
}
