#include "libft.h"

char	*ft_fill_dst(int i, char *s2char, char *dst)
{
	int	j;

	j = 0;
	while (s2char[j] != '\0')
	{
		dst[i] = s2char[j];
		i++;
		j++;
	}
	dst[i] = '\0';
	return (dst);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	char	*dst;
	char	*s1char;
	char	*s2char;

	if (!s1 || !s2)
		return (NULL);
	s1char = (char *)s1;
	s2char = (char *)s2;
	dst = (char *)malloc((ft_strlen(s1char)
				+ ft_strlen(s2char)) * sizeof(char) + 1);
	if (!dst)
		return (NULL);
	i = 0;
	while (s1char[i] != '\0')
	{
		dst[i] = s1char[i];
		i++;
	}
	dst = ft_fill_dst(i, s2char, dst);
	return (dst);
}
