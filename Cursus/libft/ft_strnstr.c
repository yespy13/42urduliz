#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	len2;
	char	*needlechar;

	needlechar = (char *) needle;
	if (*needle == '\0')
		return ((char *)haystack);
	len2 = ft_strlen(needlechar);
	while (*haystack != '\0' && len-- >= len2)
	{
		if (*haystack == *needle && ft_strncmp(haystack, needle, len2) == 0)
			return ((char *)haystack);
		haystack++;
	}
	return (NULL);
}
