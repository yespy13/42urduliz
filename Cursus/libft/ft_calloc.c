#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	int	*memory;

	memory = (int *)malloc(count * size);
	if (memory == NULL)
		return (NULL);
	ft_bzero(memory, size * count);
	return (memory);
}
