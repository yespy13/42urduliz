#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*aux;
	size_t	i;
	size_t	size;
	char	*s1char;

	s1char = (char *) s1;
	size = ft_strlen(s1char);
	aux = (char *)malloc((size + 1) * sizeof(char));
	if (aux == NULL)
		return (NULL);
	i = 0;
	while (i < size)
	{
		*(aux + i) = *(s1 + i);
		i++;
	}
	aux[i] = '\0';
	return (aux);
}
