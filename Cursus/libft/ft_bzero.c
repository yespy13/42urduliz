#include "libft.h"
void	*ft_bzero(void *s, size_t n)
{
	size_t			i;
	unsigned char	*schar;

	schar = s;
	i = 0;
	while (i < n)
	{
		if (schar[i] != 0)
			schar[i] = 0;
		i++;
	}
	return (schar);
}
