#include "libft.h"
void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t	i;
	char	*dstchar;
	char	*srcchar;

	if (dst == NULL && src == NULL)
		return (NULL);
	dstchar = (char *)dst;
	srcchar = (char *)src;
	i = 0;
	if (dstchar > srcchar)
		while (len-- > 0)
			dstchar[len] = srcchar[len];
	else
	{
		while (i < len)
		{
			dstchar[i] = srcchar[i];
			i++;
		}
	}
	return (dst);
}
