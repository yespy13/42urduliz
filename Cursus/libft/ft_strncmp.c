#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t			i;
	unsigned char	*s1char;
	unsigned char	*s2char;

	if (n == 0)
		return (0);
	s1char = (unsigned char *) s1;
	s2char = (unsigned char *) s2;
	i = 0;
	while (s1char[i] == s2char[i] && s1char[i] != '\0'
		&& s2char[i] != '\0' && i < (n - 1))
	{
		i++;
	}
	return (s1char[i] - s2char[i]);
}
