#include "libft.h"
void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*srcchar;
	unsigned char	*dstchar;
	unsigned char	cchar;

	i = 0;
	srcchar = (unsigned char *)src;
	dstchar = (unsigned char *)dst;
	cchar = (unsigned char)c;
	while (i < n)
	{
		dstchar[i] = srcchar[i];
		if (srcchar[i] == cchar)
			return (dst + i + 1);
		i++;
	}
	return (NULL);
}
