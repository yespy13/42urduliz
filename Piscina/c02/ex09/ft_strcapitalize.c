int	ft_char_is_lowercase(char c)
{
	if (c < 'a' || c > 'z')
	{
		return (0);
	}
	return (1);
}

int	ft_str_is_printable_not_alphanumeric(char c)
{
	if ((c > 47 && c < 58)
		|| (c > 64 && c < 91)
		|| (c > 96 && c < 123)
		|| c > 127)
	{
		return (0);
	}
	return (1);
}

int	ft_check_minus_to_mayus(int i, char *str)
{
	if ((i == 0 || ft_str_is_printable_not_alphanumeric(str[i - 1]) == 1)
		&& ft_char_is_lowercase(str[i]) == 1)
	{
		return (1);
	}
	return (0);
}

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (ft_check_minus_to_mayus(i, str) == 1)
		{
			str[i] -= 32;
		}
		else if (!(i == 0
				|| ft_str_is_printable_not_alphanumeric(str[i - 1]) == 1)
			&& (str[i] >= 'A' && str[i] <= 'Z'))
		{
			str[i] += 32;
		}
		i++;
	}
	return (str);
}
