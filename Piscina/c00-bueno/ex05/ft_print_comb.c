#include <unistd.h>

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_print(int i, int j, int k)
{
	ft_putchar(i);
	ft_putchar(j);
	ft_putchar(k);
}

void	ft_print_coma_espacio(void)
{
	ft_putchar(',');
	ft_putchar(' ');
}

void	ft_print_loop(int i, int j, int k)
{
	while (i < ('7' + 1))
	{
		while (j < ('8' + 1))
		{
			while (k < ('9' + 1))
			{
				ft_print(i, j, k);
				if (i == '7')
				{
					return ;
				}
				ft_print_coma_espacio();
				k++;
			}
			j++;
			k = j + 1;
		}
		i++;
		j = i + 1;
		k = j + 1;
	}
}

void	ft_print_comb(void)
{
	int	i;
	int	j;
	int	k;

	i = '0';
	j = '1';
	k = '2';
	ft_print_loop(i, j, k);
}
